from SOM import SOM
import functions as fun
import matplotlib.pyplot as plt
import numpy as np
import sys

somType = sys.argv[1]#type kohonen or gass as argument

##jedna figura
trSet = fun.generateRing(-2, 4, 4, 250)

neuronNum = 2

colors = iter(plt.cm.rainbow(np.linspace(0, 1, len(range(20)))))

plt.figure(0)

while(neuronNum < 21):
    error = []
    k = SOM(neuronNum, trSet, somType)
   
    for i in range(50):
        k.learn()
        error.append(k.Error)
    
    plt.plot(error, color=next(colors), label="n = {}".format(neuronNum))
    neuronNum += 2
    print(neuronNum)

plt.title('Zmiana bledow dla n neu, jedna figura')
plt.xlabel('Iteracja')
plt.ylabel('Blad kwantyzacji')
plt.legend()
plt.grid()

##dwie figury
trSet2 = fun.generateRing(-2, 4, 2, 150) + fun.generateRing(2, -4, 2, 150)
    
neuronNum = 2

colors = iter(plt.cm.rainbow(np.linspace(0, 1, len(range(20)))))

plt.figure(1)

while(neuronNum < 21):
    error2 = []
    k = SOM(neuronNum, trSet2, somType)
    
    for i in range(50):
        k.learn()
        error2.append(k.Error)
   
    plt.plot(error2, color=next(colors), label="n = {}".format(neuronNum))
    neuronNum += 2
    print(neuronNum)

plt.title('Zmiana bledow dla n neu, dwie figury')
plt.xlabel('Iteracja')
plt.ylabel('Blad kwantyzacji')
plt.legend()
plt.grid()
    
plt.show()   
