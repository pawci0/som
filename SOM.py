import numpy as np
import functions as fun
import matplotlib.pyplot as plt
from scipy.spatial import Voronoi, voronoi_plot_2d
import math as mt


class SOM:
    NeuronList = []

    def __init__(self, noNeurons, trainingSet, SOMtype='kohonen',
				 learnRate=0.5, radius=1, doGif=False):
        """
        Desc: creates an instance of the SOM class
        Args:
            noNeurons - number of nerons IN EACH DIMENTION of the network
            (eg: 1D: 5, 2D: 5x5)
            trainingSet - the training SetDimentions
            SOMtype - the type of the network (either "kohonen" or "gass")
        """
        # determines how many dimentions the training set has
        self.SetDimentions = len(trainingSet[0])
        # here we store the index of the closest neuron to the previous points
        # (exhaustion mechanics)
        self.LastWinner = -1
        # one of the learning coefficient
        self.Radius = float(radius)
        # one of the learning coefficient
        self.LearnRate = float(learnRate)
        # the type of the net (either "kohonen" or "gass")
        self.NetType = SOMtype
        # store the training set inside the object
        self.TrainingSet = trainingSet
        # determines the topology of the network
        self.NetDimentions = 1
        self.NeuronList = []
        self.Error = 0
        self.DoGif = doGif
        self.UsedNeurons = [0 for i in range(noNeurons)]

        if(doGif):
            self.Animation = fun.Gif()

        for n in range(noNeurons):
            self.NeuronList.append(
                self._randomPoint(self.SetDimentions, -10, 10))

    def _randomPoint(self, dim=2, lo=-10, hi=10):
        """
        Desc: generate a random point inside dim-dimentional space
        within lo/hi boundaries on each axis
        Args:
            dim - number of dimentions (default=2)
            lo - minimal value inclusive (default=-10)
            hi - maximal value exclusive (default=10)
        Returns: instance of Point class
        """
        return list(np.random.uniform(lo, hi, dim))

    def _closestNeuron(self, point):
        """
        Desc: find the closest neuron to the given point that isn't "exhausted"
        Args:
            point - point to which the distance will be measured
        Returns: coordinates of the closest neuron
        """
        minimal = float("inf")

        for index in range(len(self.NeuronList)):
            tempDist = fun.euclideanDistance(self.NeuronList[index], point)
            if((tempDist <= minimal) and (index != self.LastWinner)):
                minimal = tempDist
                minIndex = index

        self.Error += np.sqrt(minimal * minimal)
        self.LastWinner = minIndex
        # print('zwyciesca: ' + str(minIndex))
        return self.NeuronList[minIndex]

    def _closestNeuronIndex(self, point):
        minimal = float("inf")

        for index in range(len(self.NeuronList)):
            tempDist = fun.euclideanDistance(self.NeuronList[index], point)
            if((tempDist <= minimal) and (index != self.LastWinner)):
                minimal = tempDist
                minIndex = index

        self.Error += np.sqrt(minimal * minimal)
        self.LastWinner = minIndex
        # print('zwyciesca: ' + str(minIndex))
        return minIndex

    def _changePositions(self, winner, point):
        """
        Desc: adjusts position of all neurons
        Args:
            point - current point in training set
            winner - the closest neuron to the considered point
        """

        if(self.NetType == 'gass'):#if gass, let's sort array by distance to point
            sorted(self.NeuronList,
                key=lambda neuron: fun.euclideanDistance(neuron, point))

        for neuron in self.NeuronList:

            learnMul = self._learningMultiplier(winner, neuron)

            delta = [(point[i] - neuron[i]) * self.LearnRate * learnMul
                    for i in range(len(neuron))]



            for i in range(len(neuron)):
                neuron[i] += delta[i]

    def _neuronDistance(self, winner, otherNeuron):
        """
        Desc: counts the distance between neurons in the network topology
        Args:
            winner - the closest neuron to the current considered point
            otherNeuron - the neuron to which you want the distance counted
        Returns: distance
        """
        if(self.NetDimentions == 1):
            neurDistance = abs(self.NeuronList.index(otherNeuron)
                               - self.NeuronList.index(winner))
        else:
            neurDistance = fun.euclideanDistance(otherNeuron, winner)

        return neurDistance

    def _learningMultiplier(self, winner, neuron):
        """
        Desc: counts the learning multiplier for the given neuron,
        depending on the NetType, different neighbourhood functions are used
        Args:
            winner - the closest neuron to the current considered point
            neuron - the neuron for which you want the multiplier counted
        Returns: the multiplier
        """
        neurDistance = self._neuronDistance(winner, neuron)
        learnMul = fun.gaussianNeigh(self.Radius, neurDistance)

        return learnMul

    def learn(self):
        """
        Desc: iterate once over the training set and adjust the weights
        (coordinates) of the neurons, after that decrese the Radius (/=1.1)
        """
        self.UsedNeurons = [0 for i in range(len(self.NeuronList))]

        if(self.DoGif):
            self.self.Animation.append(self.NeuronList, self.TrainingSet)

        self.Error = 0

        for point in self.TrainingSet:
            self._changePositions(self._closestNeuron(point), point)
            self.UsedNeurons[self.LastWinner] = 1

        self.Error /= len(self.TrainingSet)
        self.Radius /= 1.5
        self.LearnRate /= 1.05
        

    def show(self):
    
        aliveList = [n for index, n in enumerate(self.NeuronList) if self.UsedNeurons[index] == 1]

        neuronsX, neuronsY = fun.disjoint(self.NeuronList)
        pointsX, pointsY = fun.disjoint(self.TrainingSet)

        plt.plot(pointsX, pointsY, '.k')
        plt.plot(neuronsX, neuronsY, 'r')

        plt.xlim(-10, 10)
        plt.ylim(-10, 10)
        plt.grid()

        plt.show()

    def showVoronoi(self):
    

        l = [[-1000, -1000], [1000, 1000]]
        voronoi = self.NeuronList + l
        vor = Voronoi(voronoi)
        pointsX, pointsY = fun.disjoint(self.TrainingSet)

        neuronsX, neuronsY = fun.disjoint(self.NeuronList)
        
        voronoi_plot_2d(vor, show_vertices=False)

        plt.xlim(-10, 10)
        plt.ylim(-10, 10)
        plt.grid()

        plt.figure(1)
        plt.plot(pointsX, pointsY, '.k')
        plt.plot(neuronsX, neuronsY, 'r')

        plt.show()

    def saveAnimation(self, name):

        self.Animation.save(name)

    def countDead(self):

        number = 0
        for index in range(len(self.NeuronList)):
            if(self.UsedNeurons[index] == 0):
                number += 1

        return number
