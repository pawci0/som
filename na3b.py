from SOM import SOM
import functions as fun
import matplotlib.pyplot as plt
import numpy as np
import sys

somType = sys.argv[1]#type kohonen or gass as argument

def countError(trainingSet, learnRate = 0.5):

	
	howMany = 10
	learnCycles = 50
	error = 0
	
	for i in range(howMany):

		network = SOM(20, trainingSet, somType)
		network.learnRate = learnRate
	
		for j in range(learnCycles):
		
			network.learn()
		
		error += network.Error
		
	error /= howMany
	
	return error
	
	
	
###############################################main

trainSet = fun.generateCircle(2, 2, 3, 250)

print(countError(trainSet, 0.5))
print(countError(trainSet, 0.2))
