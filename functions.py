from imageio import imread, mimsave
from tempfile import mkstemp, mkdtemp
import numpy as np
from scipy.spatial import distance
import matplotlib.pyplot as plt
import copy as cp


# generuje nPoints punktow / centroidow dla odpowiednio switch = 0 / switch = 1
def generateCircle(ox, oy, R, nPoints):

    def _generatePoint(ox, oy, R):
        t = 2 * np.pi * np.random.uniform(0, 1)
        r = np.random.uniform(0, R)
        px = ox + r * np.cos(t)
        py = oy + r * np.sin(t)
        return [px, py]

    return [_generatePoint(ox, oy, R) for point in range(nPoints)]


def generateRing(ox, oy, R, nPoints):

    def pointOnCircumference(x, y, r):
        alfik = np.random.random() * 2 * np.pi
        return x + np.cos(alfik) * r, y + np.sin(alfik) * r

    return [pointOnCircumference(ox, oy, R)
            for point in range(nPoints)]


def generateSquare(ox, oy, R, nPoints):
    ans = []
    while len(ans) < nPoints:
        switch = np.random.choice([0, 1])
        if(switch == 1):
            x = np.random.uniform(oy - R, oy + R)
            y = np.random.choice([ox - R, ox + R])
        else:
            x = np.random.choice([oy - R, oy + R])
            y = np.random.uniform(ox - R, ox + R)
        ans.append([x, y])

    return ans


def euclideanDistance(arr1, arr2):
    return distance.euclidean(arr1, arr2)


def gaussianNeigh(radius, distance):
    return np.exp(- (distance**2) / (2 * radius**2))


def disjoint(listXY):
    arrX = []
    arrY = []

    for point in listXY:
        arrX.append(point[0])
        arrY.append(point[1])

    return arrX, arrY


def error(neurons, points):

    error = 0

    for point in points:
        for neuron in neurons:
            error += euclideanDistance(neuron, point)

    return error / len(neurons)


class Gif:

    def __init__(self):

        self.tempdir = mkdtemp()
        self.frames = []

###########################################

    def append(self, neurons, points):

        neuronsX, neuronsY = disjoint(neurons)
        pointsX, pointsY = disjoint(points)

        plt.plot(pointsX, pointsY, '.k')
        plt.plot(neuronsX, neuronsY, 'r')

        plt.xlim(-10, 10)
        plt.ylim(-10, 10)
        plt.grid()

        _, filename = mkstemp(dir=self.tempdir)
        filename += '.png'
        plt.savefig(filename)
        plt.clf()

        self.frames.append(filename)

#################################################

    def save(self, name, dur=0.5):

        frames = []

        for filename in self.frames:
            img = imread(filename)
            frames.append(img)

        mimsave(name, frames, duration=dur)

class Change:

    def __init__(self, howManyNeurons, trSet):
        self.Lists = []
        self.TrainingSet = trSet

        for list in range(howManyNeurons):
            newList = []
            self.Lists.append(newList)

    def appendEpoch(self, neurons):
        plt.figure(4)
        for index in range(len(neurons)):
            self.Lists[index].append(cp.deepcopy(neurons[index]))

    def plot(self):
        colors = iter(plt.cm.rainbow(np.linspace(0, 1, len(self.Lists))))

        for l in self.Lists:
            x, y = disjoint(l)
            c = next(colors)
            plt.plot(x, y, color=c, linestyle='--', marker='.')

            plt.plot(x[0], y[0], color=c, marker='x', markersize=7)
            plt.plot(x[-1], y[-1], color=c, marker='X', markersize=7)

        x, y = disjoint(self.TrainingSet)
        plt.plot(x, y, ".k", markersize=5)

        plt.title("Trajektoria zmian polozenia neuronow")

        plt.grid()
        plt.xlim(-10, 10)
        plt.ylim(-10, 10)
        plt.show()
