from SOM import SOM
import functions as fun
import matplotlib.pyplot as plt
import numpy as np
import sys
import random as rand


somType = sys.argv[1]#type kohonen or gass as argument
lr = sys.argv[3]
r = sys.argv[4]

if sys.argv[2] == "-a":

##jedna figura
    trSet = fun.generateRing(-2, 4, 4, 250)

    neuronNum = 2

    colors = iter(plt.cm.rainbow(np.linspace(0, 1, len(range(20)))))

    plt.figure(0)

    while(neuronNum < 22):
        error = []
        k = SOM(neuronNum, trSet, somType, lr, r)

        for i in range(50):
            k.learn()
            error.append(k.Error)

        plt.plot(error, color=next(colors), label="n = {}".format(neuronNum))
        neuronNum += 2
        print(neuronNum)

    plt.title('Zmiana bledow dla n neuronow, jedna figura')
    plt.xlabel('Iteracja')
    plt.ylabel('Blad kwantyzacji')
    plt.legend()
    plt.grid()

##dwie figury
    trSet2 = fun.generateRing(-2, 4, 2, 150) + fun.generateRing(2, -4, 2, 150)

    rand.shuffle(trSet2)
    
    neuronNum = 2

    colors = iter(plt.cm.rainbow(np.linspace(0, 1, len(range(20)))))

    plt.figure(1)

    while(neuronNum < 22):
        error2 = []
        k = SOM(neuronNum, trSet2, somType)

        for i in range(50):
            k.learn()
            error2.append(k.Error)

        plt.plot(error2, color=next(colors), label="n = {}".format(neuronNum))
        neuronNum += 2
        print(neuronNum)

    plt.title('Zmiana bledow dla n neuronow, dwie figury')
    plt.xlabel('Iteracja')
    plt.ylabel('Blad kwantyzacji')
    plt.legend()
    plt.grid()

    plt.show()
##end -a

elif sys.argv[2] == "-e": #costam
    oneFigure = fun.generateRing(3, 3, 5, 201)

    minErr = float("inf")
    errList = []
    deadNeurons = []

    for i in range(20):
        siec = SOM(20, oneFigure, somType, lr, r)

        for j in range(20):
            siec.learn()

        errList.append(siec.Error)
        deadNeurons.append(siec.countDead())

        if(siec.Error < minErr):
            minErr = siec.Error

    #    print(i)

    avgDead = np.average(deadNeurons)
    stdDead = np.std(deadNeurons, ddof=1)

    avgErr = np.average(errList)
    stdErr = np.std(errList, ddof=1)

    #print("JEDNA FIGURA: avgErr: " + str(avgErr) + "\nminErr: " + str(minErr) + "\nstdErr: " + str(stdErr) + "\navgDead: " + str(avgDead) + "\nstdDead: " + str(stdDead))
    print("Jedna:\n")
    print(str(lr) + " & " + str(r) + " & " + str(avgErr)[:7] + " & " + str(stdErr)[:7] + " & " + str(minErr)[:7] + " & " + str(avgDead)[:7] + " & " + str(stdDead)[:7])

    firstFigure = fun.generateSquare(-5, -5, 2, 101)
    secondFigure = fun.generateRing(3, 3, 5, 101)

###### dwie figury
    
    twoFigures = firstFigure + secondFigure

    rand.shuffle(twoFigures)

    
    minErr = float("inf")
    errList = []
    deadNeurons = []

    for i in range(20):
        siec = SOM(20, oneFigure, somType, lr, r)

        for j in range(20):
            siec.learn()

        errList.append(siec.Error)

        deadNeurons.append(siec.countDead())

        if(siec.Error < minErr):
            minErr = siec.Error

     #   print(i)

    avgDead = np.average(deadNeurons)
    stdDead = np.std(deadNeurons, ddof=1)

    avgErr = np.average(errList)
    stdErr = np.std(errList, ddof=1)

    #print("\n\nDWIE FIGURY: avgErr: " + str(avgErr) + "\nminErr: " + str(minErr) + "\nstdErr: " + str(stdErr) + "\navgDead: " + str(avgDead) + "\nstdDead: " + str(stdDead))

    print("Dwie:\n")
    print(str(lr) + " & " + str(r) + " & " + str(avgErr)[:7] + " & " + str(stdErr)[:7] + " & " + str(minErr)[:7] + " & " + str(avgDead)[:7] + " & " + str(stdDead)[:7])


##end -e

elif sys.argv[2] == "-c":
    oneFigure = fun.generateRing(-5, 5, 2, 201)

    NO_NEURONS = 10


    siec = SOM(NO_NEURONS, oneFigure, somType, lr, r)

    ch = fun.Change(NO_NEURONS, siec.TrainingSet)

    for i in range(20):
        ch.appendEpoch(siec.NeuronList)

        siec.learn()

    ch.appendEpoch(siec.NeuronList)
    siec.show()
    ch.plot()
    siec.showVoronoi()
    

    
##### dwie figury
    
    twoFIgures = fun.generateRing(-5, 5, 3, 150) + fun.generateSquare(-5, 5, 2, 150)
    
    rand.shuffle(twoFIgures)

    siec = SOM(NO_NEURONS, twoFIgures, somType, lr, r)


    ch = fun.Change(NO_NEURONS, siec.TrainingSet)

    for i in range(20):
        ch.appendEpoch(siec.NeuronList)
        siec.learn()

    ch.appendEpoch(siec.NeuronList)
    siec.show()
    #print(siec.NeuronList)
   
    ch.plot()
    siec.showVoronoi()
